-- MySQL dump 10.13  Distrib 5.5.62, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: tmexiot_db
-- ------------------------------------------------------
-- Server version	5.5.62-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_tmexiot_user`
--

DROP TABLE IF EXISTS `tbl_tmexiot_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tmexiot_user` (
  `tmexiot_user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reg_user` varchar(45) DEFAULT NULL,
  `reg_pass` varchar(45) DEFAULT NULL,
  `reg_pass_conf` varchar(45) DEFAULT NULL,
  `reg_fecha` datetime DEFAULT NULL,
  `reg_admin` varchar(45) DEFAULT NULL,
  `reg_passadmin` varchar(45) DEFAULT NULL,
  `reg_usu` varchar(45) DEFAULT NULL,
  `reg_passusu` varchar(45) DEFAULT NULL,
  `reg_serv1` int(11) DEFAULT NULL,
  `reg_serv2` int(11) DEFAULT NULL,
  PRIMARY KEY (`tmexiot_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tmexiot_user`
--

LOCK TABLES `tbl_tmexiot_user` WRITE;
/*!40000 ALTER TABLE `tbl_tmexiot_user` DISABLE KEYS */;
INSERT INTO `tbl_tmexiot_user` VALUES (1,'A','aaa','aaa',NULL,'admin','admin','usu','usu',1,NULL),(2,'a','a','a',NULL,'a','a','usu','usu',1,NULL),(3,'a@a.com','a','a',NULL,'a','a','usu','usu',1,NULL);
/*!40000 ALTER TABLE `tbl_tmexiot_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-26 19:54:21
