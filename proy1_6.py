#Este archivo se encarga de realizar las tareas del menú "Corte".

#>>>>>>>>>>>>>>>>>>>>> Paqueteria importada <<<<<<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

import fileinput
import numpy as np#instalable
#from Tkinter import *#instalable
from tkinter import ttk#instalable
import mysql.connector
from tkinter import *
from tkinter.scrolledtext import ScrolledText
import datetime
from datetime import datetime
from datetime import timedelta
from datetime import date
from math import *
import os, sys
#from ScrolledText import * #instalable
from tkcalendar import DateEntry #instalable
from werkzeug import generate_password_hash, check_password_hash


#>>>>>>>>>>>>>>>>>>> Menú Corte <<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
#Esta función corresponde al menú "Corte". Este realiza corte del d[ia mostrando un resumen del total de coche y dinero  manejado en ese día. 

#$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$$%$%$%$


def corte(root, cursor, conn, fields4):
	status = Label(root, text="Haciendo corte, espere.", bd=1, relief=SUNKEN)

	status.grid(row=12,column=0,columnspan=4,rowspan=4)
	status.config(width=82)

	VAR = 0
	#with open ("Lista.csv", "r") as archivo:
	#	lista = archivo.readlines()
	#lista = [x.strip() for x in lista]
	b = date.today()
	#print(s)
	print("b = ", b)
	b2 =  b + timedelta(days=1)  
	print("b2 = ", b2)
	val = (b, b2)
	cursor.execute("SELECT * FROM tbl_esta_reg WHERE  reg_checkin >= %s and reg_checkin <= %s", val)
	res = cursor.fetchall()
	#print('res = ',res,'np.size ', np.shape(res))
	datos_dict = []
	datos_dict2 = []
	#txbx.delete('1.0','end')
	duracion3 = 0
	duracion4 = 0
	cost = 0
	coches1 = 0
	coches2 = 0
	for i in res:
		#print (i)
		"""dict_["id"] = i[0]
		dict_["cb"] = i[1]
		dict_["Nombre"] = i[2]
		dict_["telefono"] = i[3]
		dict_["Modelo"] = i[4]
		dict_["color"] = i[5]
		dict_["placa"] = i[6]
		dict_["pension"] = i[7]
		dict_["costo"] = i[8]
		dict_["pagado"] = i[9]
		dict_["in"] = i[10]
		dict_["out"] = i[11]
		dict_["coment"] = i[12]
		dict_["costoT"] = i[13]
		datos_dict.append(dict_)"""

		if i[9] == "NO":
			
			if i[7] == '0' or i[7] == 0:
				print("hora adentro", i[8])
				dur1 = datetime.now()-i[10]
				cost1 = ceil(int(dur1.days)*24 + int(dur1.seconds)/3600)*float(i[8])
				cost = cost + cost1
				coches1 = coches1 + 1
				print(cost1) 
				print(dur1)
				#txbx.insert("1.0", (i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora\t\t" + str(i[10]) + "\t\t---------NA---------\t\t "+str(cost1)+" \t Sin comentario\n"),"d1")
				#txbx.tag_config('d1', foreground='blue')  

				
				
			if i[7] == '1' or i[7] == 1:
				print("pension adentro", i[8])
				dur1 = datetime.now()-i[10]
				dur2 =   datetime.now()-i[10]
				cost2 = ceil(int(dur1.days) + int(dur1.seconds)/(3600*24))*float(i[8])
				cost = cost + cost2
				coches1 = coches1 + 1
				print(cost2)
				print(dur2)
				#txbx.insert("1.0",( i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día\t\t" + str(i[10]) + "\t\t----------NA----------\t\t "+str(cost2)+" \t Sin comentario\n"), "d2")
				#txbx.tag_config('d2', foreground='gray')
				
		else:
			
			if i[7] == '0' or i[7] == 0:
				coches2 = coches2 + 1
				print("hora afuera")
				#dur3 = i[11] - i[10]
				#print(dur3)
				if i[9] == "C":
					try:
						#txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t "+i[12]+"\n")
						1+1
					except:
						#txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
						1+1
					try:
						duracion3 = i[13] + duracion3
					except:
						duracion3 = duracion3
				elif i[9] == "SI":
					#txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
					duracion3 = i[13] + duracion3
			if i[7] == '1' or i[7] == 1:
				coches2 = coches2 + 1
				print("pension afuera")
				#dur4 = i[11] - i[10]
				#print(dur4)
				if i[9] == "C":
					
					try:
						#txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t "+i[12]+"\n")
						1+1
					except:
						#txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
						1+1
					try:
						duracion3 = i[13] + duracion3
					except:
						duracion3 = duracion3
				elif i[9] == "SI":
					#txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
					duracion3 = i[13] + duracion3
	
	#print(datos_dict2)		
	#print(datos_dict2)
	#txbx.insert("1.0", datos_dict2)
	#txbx.bind("<Enter>", enter)
	
	texto = ""	
		
		
		#'Nombre\t\tModelo\t\tPlaca\t\tCosto/pensiont\tFecha ingreso\t\tFecha salida'
	#i = 3
	#var = np.size(lista)-i
	#b1 = str(b[0]).split('-')
	#b2 = str(b[1]).split('-')
	#a1 = ','.join(b1)
	#a2 = ','.join(b2)
	#b1_ = to_date(a1,VAR)
	#b2_ = to_date(a2,VAR)
	
	duracion1 = []
	duracion2 = []
	
	#txbx.delete('1.0','end')
	
	print("coches adentro: ",coches1, " coches afuera: ", coches2, " costo por cobra: ", cost, " costo recaudado: ", duracion3)
	
	#x,y = Total(duracion1,duracion2,duracion3,duracion4)
	#tex = '    %i\n    %i'%(coches1, coches2)
	#Label(window,  text=tex, font=(16), fg="red").grid(padx=5, pady=5, row=6, column=0,columnspan = 1, rowspan = 2,  sticky =  (E)) 
	#tex = '        $%.2f \n        $%.2f'%(cost, duracion3)
	#Label(window,  text=tex, font=(16), fg="red").grid(padx=5, pady=5, row=6, column=1, rowspan = 2,  sticky =  (E)) 
	#text = '______________________________________________________________________________________________________________\n'
	#txbx.insert('1.0',text)
	#print 'duracion :',duracion1,np.shape(duracion3),np.shape(duracion2)[0],np.shape(duracion4)
	"""tex = 'Hay:\t %i coches en modo por hora adentro \n\t %i coches en modo pension adentro \nSalieron:\t %i coches en modo por hora \n\t %i coches en modo pensión'%(np.size(duracion1), np.size(duracion3), np.size(duracion2), np.size(duracion4))
	""" 
















