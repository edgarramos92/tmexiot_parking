#Este archivo se encarga de realizar las tareas del menú "configuración".

#>>>>>>>>>>>>>>>>>>>>> Paqueteria importada <<<<<<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

import fileinput
import numpy as np#instalable
#from Tkinter import *#instalable
from tkinter import ttk#instalable
import mysql.connector
from tkinter import *
from tkinter.scrolledtext import ScrolledText
import datetime
from datetime import datetime
from math import *
import os, sys
#from ScrolledText import * #instalable
from tkcalendar import DateEntry #instalable
from werkzeug import generate_password_hash, check_password_hash
from proy1_8 import stringchk3
from proy1_8 import stringchk


#>>>>>>>>>>>>>>>>>>> Menú confguración <<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
#Esta función corresponde al menú "Configuración". Este despliega una ventana para editar los datos del lugar, 


def window1(fields0,conn2, cursor2, results, conn,cursor, root):
	global valo3 
	#print(type(valo3))
	#print("Id de usuario: ",results[0][0])
	entries = [] 	#Para almacenar el contenido de los entries de nombres y costos
	entrie = []		#Para almacenar el usuario y la contrasena nueva.
#Inicializando tkinter:
	window = Toplevel(root)
	
	#window.geometry("400x300")
	#marco = Frame(window)
	window.title('Configurar')
	#print(fields0,fields)

#Aquí se crea el status bar
	status = Label(window, text='Es necesario introducir su contraseña actual para realizar los cambios.', bd=1, relief=SUNKEN)
	
	status.grid(row=14,column=0,columnspan=4,rowspan=1)
	status.config(width=65)

#Aquí se crea el texto que va hasta la parte superior de la ventana
	Label(window, width=15, text="Introduce los datos que quieras editar: nombre y costos del estacionamiento", anchor='w').grid(padx=5, pady=5, row=0, column=0, columnspan = 4, sticky = (N, S, E, W))	
	Label(window, width=15, text=" o datos de acceso para el administrador y usuario.", anchor='w').grid(padx=5, pady=5, row=1, column=0, columnspan = 4,  sticky = (N, S, E, W))	

#Aquí se crea el label y el entry para escribir la contraseña actual y realizar los cambios
	Label(window, width=8, text="Contraseña actual:", anchor='w').grid(padx=5, pady=5, row=13, column=0, sticky = (N, S, E, W))
	ents = Entry(window, width=12,show='*')
	ents.grid(padx=5, pady=5, row=13, column=1,columnspan = 2, sticky = (N, S, E, W))

#Aquí se crean los label y entry de la configuracion delnombre y costos.

	for i,j in zip(fields0,range(np.size(fields0))):
		Label(window, width=8, text=i, anchor='w').grid(padx=5, pady=5, row=j+2, column=0, sticky = (N, S, E, W))
		ent = Entry(window, width=12)
		ent.grid(padx=5, pady=5, row=j+2, column=1, columnspan = 2, sticky = (N, S, E, W))
		
		entries.append((i, ent))
	
#Aquí se crean los entrys y label para cambiar el nombre de usuario de admin y  del usuario. También cambias el password de cualquera de los dos.
	
	

	
	
#Aquí se crea el combobox y el entry para elegir si cambiar user o admin
	
	valo3 = StringVar()
	list_combo = "Administrador","Usuario"
	combo =  ttk.Combobox(window,width=8,values =list_combo , textvariable=valo3 )
	combo.grid(padx=5, pady=5,row=6, column = 0, sticky=(N, S, E, W))
	#combo.current(0)
	ent = Entry(window, width=12)
	ent.grid(padx=5, pady=5, row=6, column=1, columnspan = 2, sticky = (N, S, E, W))
	entrie.append((valo3, ent))
	#print("El valor es : ",valo3.get())	
#Aquí se crean los label y entry de conraseña y confirmar contraseña
	fields0 = [ "Contrasena:", "Confirmar contraseña:"]
	for i,j in zip(fields0,range(np.size(fields0))):
		if j >= 0:
			Label(window, width=8, text=i, anchor='w').grid(padx=5, pady=5, row=j+8, column=0, sticky = (N, S, E, W))
			ent = Entry(window, width=12, show= "*")
			ent.grid(padx=5, pady=5, row=j+8, column=1, columnspan = 2, sticky = (N, S, E, W))
		entrie.append((i, ent))

#Aquí se crean los botones de guardar (configuracion) y actualizar (user-pass).
	b1 = Button(window, width=8, text='Guardar',command=( lambda s = entries: savdat(s, conn2, cursor2,ents,results,window, conn,cursor,fields0,list_combo))).grid(padx=5, pady=5, row=5, column=1, columnspan = 2, rowspan = 1, sticky = (N, S, E, W))
	b2 = Button(window, width=8, text='Actualizar',command=(lambda s = entrie: savdat(s, conn2, cursor2,ents,results,window, conn,cursor,fields0, list_combo))).grid(padx=5, pady=5, row=12, column=1, columnspan =2, rowspan = 1, sticky = (N, S, E, W))
	

#>>>>>>>>>>>>>>>>>>>>>> Guardar / Actualizar <<<<<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
#Esta función corresponde al menú "Configuración". Este despliega una ventana para editar los datos del lugar, 




def savdat(s, conn2, cursor2,ents, results,window,conn,cursor,fields0,list_combo ):
	ents2 = []
	ents2.append((ents,ents))
	ents2.append((ents,ents))
	#print(ents.get(),s)
	varstr = stringchk(ents2)
	varstr2 = stringchk3(s)
	

	now = datetime.now()
	rand = []
	#print(dir(valo3))
	#print("valo3", valo3, valo3.get(),type(valo3), sys.getsizeof(valo3))
	if ents.get() != "" :

		if varstr and varstr2:
			#print('resultados:',results, 's = :',s, 'fields0 ',fields0 )
			#print('s00.get : ', s[0][0].get(),'s00 : ', s[0][0])
			val = (results[0][0],)
			cursor2.execute("SELECT  reg_passadmin FROM tbl_tmexiot_user WHERE tmexiot_user_id = %s  ",val)
			res = cursor2.fetchall()
			#print(res)
			#print('res ',res,' s = ', s[0][0].get())
			if check_password_hash(res[0][0], ents.get()) and s[0][0] == "Nombre:": #agregar un else por si la contrasena es incorrecta
				#print(ents.get())
				#rand = []
				
		
				for i in s:
					rand.append(i[1].get())
				
				#print(rand)
				if  rand[0] != '' and rand[1] != '' and rand[2] != '' :
					try:
						a = float(rand[1])
						b = float(rand[2])
						
		#and (rand[2].isdigit() or rand[2].isdecimal()) and (rand[3].isdigit() or rand[3].isdigit())
						cursor.execute("UPDATE tbl_esta_conf SET conf_vigencia = 0 WHERE conf_vigencia = 1")
						conn.commit()
						val = (rand[0], rand[1], rand[2],  "NULL")
						cursor.execute("INSERT INTO tbl_esta_conf (conf_name, conf_costo1, conf_costo2, conf_lugar) VALUES (%s, %s, %s, %s)", val)
						conn.commit()


						status = Label(window, text="Se han guardado los cambios: Nombre %s  Costo p/hora $%s  Costo pensión $%s"%(rand[0],rand[1],rand[2]), bd=1, relief=SUNKEN)

						status.grid(row=14, column=0, columnspan=4, rowspan=1)
						status.config(width=65)
						#print 'El costo elegido es: ',rand[1]
						#l.write(str(rand[0])+'\n')
						#l.write((rand[1])+'\n')
						#l.write((rand[2])+'\n')
						#m = 0
					except:
				#else:
						status = Label(window, text="Introduce los costos utilizando el teclado numérico", bd=1, relief=SUNKEN)

						status.grid(row=14, column=0, columnspan=4, rowspan=1)
						status.config(width=65)
				#	m = 1
			
				#else:
				#	print ('Introduce el nombre del estacionamiento y los costos' )
				#	m = 1
				#if m != 1 :
				#	print( 'Han sido guardados los cambios')
				
				else:
					status = Label(window, text="Introduce el nombre del lugar y los costos.", bd=1, relief=SUNKEN)

					status.grid(row=14, column=0, columnspan=4, rowspan=1)
					status.config(width=65)
				
			elif check_password_hash(res[0][0], ents.get()) and (s[0][0].get() == list_combo[0] or s[0][0].get() == list_combo[1]) or (check_password_hash(res[0][0], ents.get()) and s[0][0].get() == '' ):
				
				
				if s[1][1].get() == s[2][1].get() and s[1][1].get() != "" and s[2][1].get() != "" and s[0][1].get() != "" and s[0][0].get() == list_combo[0]:
					val = (s[0][1].get(), generate_password_hash(s[1][1].get()), results[0][0])
					cursor2.execute("UPDATE tbl_tmexiot_user SET reg_admin = %s, reg_passadmin = %s  WHERE tmexiot_user_id = %s",val)
					conn2.commit()
					status = Label(window, text="Administrador y contraseña actualizados.", bd=1, relief=SUNKEN)

					status.grid(row=14,column=0,columnspan=4,rowspan=1)
					status.config(width=65)
				elif s[1][1].get() == s[2][1].get() and s[1][1].get() != "" and s[2][1].get() != "" and s[0][1].get() != "" and s[0][0].get() == list_combo[1]:
					val = (s[0][1].get(), generate_password_hash(s[1][1].get()), results[0][0])
					
					cursor2.execute("UPDATE tbl_tmexiot_user SET reg_usu = %s, reg_passusu = %s  WHERE tmexiot_user_id = %s",val)
					conn2.commit()
					status = Label(window, text="Usuario y contraseña actualizados.", bd=1, relief=SUNKEN)

					status.grid(row=14, column=0, columnspan=4, rowspan=1)
					status.config(width=65)
				else:
					status = Label(window, text="No coinciden las contraseñas o campos vacios.", bd=1, relief=SUNKEN)

					status.grid(row=14, column=0, columnspan=4, rowspan=1)
					status.config(width=65)

			else:
				status = Label(window, text="Introduce tu contraseña actual de Administrador para realizar los cambios.", bd=1, relief=SUNKEN)
				status.grid(row=14, column=0, columnspan=4, rowspan=1)
				status.config(width=65)	
		else:
			status = Label(window, text='Consulta la lista de caracteres no admitidos en el menú Ayuda.', bd=1, relief=SUNKEN)
			status.grid(row=14, column=0, columnspan=4, rowspan=1)
			status.config(width=65)
	else:
		status = Label(window, text="Introduce tu contraseña actual de Administrador para realizar los cambios.", bd=1, relief=SUNKEN)
		status.grid(row=14, column=0, columnspan=4, rowspan=1)
		status.config(width=65)
