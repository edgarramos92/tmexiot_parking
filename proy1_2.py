#Este archivo se encarga de realizar las tareas de los botones de "Guardar" y de "Limpiar".

#>>>>>>>>>>>>>>>>>>>>> Paqueteria importada <<<<<<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

import fileinput
import numpy as np#instalable
#from Tkinter import *#instalable
from tkinter import ttk#instalable
import mysql.connector
from tkinter import *
from tkinter.scrolledtext import ScrolledText
import datetime
from datetime import datetime
from math import *
import os 
#from ScrolledText import * #instalable
from tkcalendar import DateEntry #instalable
from werkzeug import generate_password_hash, check_password_hash
from proy1_8 import stringchk
import requests
import json


#>>>>>>>>>>>>>>>>>>> Botón de Limpiar entries <<<<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
#Esta función corresponde al botón de "Limpiar" su función es limpiar los entrys de registro del cliente.
def clear_entry(root,saves,lab,dat):
	global chk
	chk = 0
	print(dat)
	status = Label(root, text="Bienvenido a %s"%dat[0][0], bd=1, relief=SUNKEN)
	chk = 0
	status.grid(row=12,column=0,columnspan=4,rowspan=4)
	status.config(width=82)
	for entry in saves:
		entry[1].delete(0,END)
	print(chk)

#>>>>>>>>>>>>>>>> Botón de Guardado de clientes <<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
#Esta funcion corresponde al botó para guardar los datos del cliente en la base de datos.
def save(root,saves,var1,val2, cursor,conn):
	
	global chk
	varstr = stringchk(saves)
	print("saves: ", saves)
	print(varstr)
	if var1.get() != '' and saves[4][1].get() != '':
		if varstr:
			#print("si lees esto es por que no tiene simbolos")
			#with open ("data.csv", "r") as archivo:
			#	dat = archivo.readlines()
			#dat = [x.strip() for x in dat]
			cursor.execute("SELECT conf_costo1, conf_costo2 FROM tbl_esta_conf WHERE conf_vigencia = 1 ")
			dat = cursor.fetchall()
			print(val2)
			#print(var1.get())
			print('1')
			for i in range(np.size(val2)):
				if var1.get() == val2[0]:
					var2 = dat[0][0]
					var = 0
				elif var1.get() == val2[1]:
					var2 = dat[0][1]
					var = 1
				elif var1.get() == val2[i]:
					var2 = float(val2[i].split(' ')[1])
					var = int(val2[i].split('\t\t')[1])
			savs = []
			now = datetime.now()
			date = int(''.join(map(str,(now.day,now.month,now.year,now.hour,now.minute,now.second))))
			indice = 0
			for i,k in zip(range(np.size(saves)),saves):
				rand = ''.join(map(str,(date,k[1].get())))
				indice = 1
			for entry,i in zip(saves,range(np.size(saves))):
				savs.append(entry[1].get())
			dict_reg = dict()
			list_dict = "CB", "name", "phone", "model", "color", "placa", "pension", "costo", "pagado", "checkin"
			for i,j in zip(list_dict,range(np.size(list_dict))):
				if j == 0:
					dict_reg[i] = rand
				if j > 0 and j < 6:
					dict_reg[i] = savs[j-1]
				if j == 6:
					dict_reg[i] = str(var)
				if j == 7:
					dict_reg[i] = str(var2)
				if j == 8:
					dict_reg[i] = 'NO'
				if j == 9:
					dict_reg[i] = now

			
			#val = (dict_reg["CB"], dict_reg["name"], dict_reg["phone"], dict_reg["model"], dict_reg["color"], dict_reg["placa"], dict_reg["pension"], dict_reg["costo"], dict_reg["pagado"], dict_reg["checkin"])
			#cursor.execute("INSERT INTO tbl_esta_reg (reg_CB,reg_name, reg_phone, reg_model, reg_color, reg_placa ,reg_pension,reg_costo,reg_pagado, reg_checkin ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", val)
			#conn.commit()

			
			url = 'http://127.0.0.1:9000'
			url_clients = url+'/parking_reg_clients?arg1='+dict_reg["CB"]+'&arg2='+dict_reg["name"]+'&arg3='+dict_reg["phone"]+'&arg4='+dict_reg["model"]+'&arg5='+dict_reg["color"]+'&arg6='+dict_reg["placa"]+'&arg7='+dict_reg["pension"]+'&arg8='+dict_reg["costo"]+'&arg9='+dict_reg["pagado"]+'&arg10='+str(dict_reg["checkin"])
			url_request = requests.get(url_clients)
			output = json.loads(url_request.text)
			print('========',output)


			status = Label(root, text='Para poder reclamar su auto es necesario presentar el siguinte número : %s'%rand, bd=1, relief=SUNKEN)
			status.grid(row=12,column=0,columnspan=4,rowspan=4)
			status.config(width=82)
			root.counter = root.counter + 1
			chk = 0
			
		else:
			status = Label(root, text='Consulta la lista de caracteres no admitidos en el menú Ayuda.', bd=1, relief=SUNKEN)
			status.grid(row=12,	column=0, columnspan=4,	rowspan=4)
			status.config(width=82)

	else:
		status = Label(root, text='Escriba la placa del coche y elija el costo.', bd=1, relief=SUNKEN)
		status.grid(row=12,column=0,columnspan=4,rowspan=4)
		status.config(width=82)