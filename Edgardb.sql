-- MySQL dump 10.13  Distrib 5.5.62, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: Edgar_db
-- ------------------------------------------------------
-- Server version	5.5.62-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_esta_conf`
--

DROP TABLE IF EXISTS `tbl_esta_conf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_esta_conf` (
  `conf_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `conf_name` varchar(45) DEFAULT NULL,
  `conf_lugar` varchar(45) DEFAULT NULL,
  `conf_costo1` float DEFAULT NULL,
  `conf_costo2` float DEFAULT NULL,
  `conf_fecha` datetime DEFAULT NULL,
  `conf_vigencia` int(11) DEFAULT '1',
  PRIMARY KEY (`conf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_esta_conf`
--

LOCK TABLES `tbl_esta_conf` WRITE;
/*!40000 ALTER TABLE `tbl_esta_conf` DISABLE KEYS */;
INSERT INTO `tbl_esta_conf` VALUES (1,'name','place',12,420,'2020-01-12 21:37:35',0),(2,'abc','NULL',420,420,NULL,1);
/*!40000 ALTER TABLE `tbl_esta_conf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_esta_conv`
--

DROP TABLE IF EXISTS `tbl_esta_conv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_esta_conv` (
  `conv_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `conv_name` varchar(45) DEFAULT NULL,
  `conv_costo` float DEFAULT NULL,
  `conv_note` varchar(100) DEFAULT NULL,
  `conv_fecha` datetime DEFAULT NULL,
  `conv_tipo` int(11) DEFAULT NULL,
  `conv_vigencia` int(11) DEFAULT '1',
  PRIMARY KEY (`conv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_esta_conv`
--

LOCK TABLES `tbl_esta_conv` WRITE;
/*!40000 ALTER TABLE `tbl_esta_conv` DISABLE KEYS */;
INSERT INTO `tbl_esta_conv` VALUES (1,'name',450,'sncsdh c\n','2020-01-12 13:04:00',1,1),(2,'name2',12.5,'\n','2020-01-12 13:05:43',0,1),(3,'name3',152,'\n','2020-01-12 13:05:55',1,1);
/*!40000 ALTER TABLE `tbl_esta_conv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_esta_reg`
--

DROP TABLE IF EXISTS `tbl_esta_reg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_esta_reg` (
  `reg_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reg_CB` varchar(45) DEFAULT NULL,
  `reg_name` varchar(45) DEFAULT NULL,
  `reg_phone` varchar(45) DEFAULT NULL,
  `reg_model` varchar(45) DEFAULT NULL,
  `reg_color` varchar(45) DEFAULT NULL,
  `reg_placa` varchar(45) DEFAULT NULL,
  `reg_pension` varchar(45) DEFAULT NULL,
  `reg_costo` varchar(45) DEFAULT NULL,
  `reg_pagado` varchar(45) DEFAULT NULL,
  `reg_checkin` datetime DEFAULT NULL,
  `reg_checkout` datetime DEFAULT NULL,
  `reg_comment` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`reg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_esta_reg`
--

LOCK TABLES `tbl_esta_reg` WRITE;
/*!40000 ALTER TABLE `tbl_esta_reg` DISABLE KEYS */;
INSERT INTO `tbl_esta_reg` VALUES (1,'151202015534HJCMND06','Edgar Ramos','3311607633','Mustang ','Rojo','HJCMND06','0','15','SI','2020-01-15 15:53:04','2020-01-15 21:28:42',NULL),(2,'1512020155357Rame921008','Rastas','3311607633','Mustang','Rojo','Rame921008','1','100','SI','2020-01-15 15:53:57','2020-01-15 17:34:19',NULL),(3,'1512020155444rame921008hjcmnd06','Rastudito','3311607633','Mercedes-Benz','Plata','rame921008hjcmnd06','1','100','C','2020-01-15 15:54:44','2020-01-15 21:36:26',NULL),(4,'15120201631HJCMND01','Edgar Ramos','3311607633','Mustang ','Rojo','HJCMND01','1','152.0','C','2020-01-15 16:03:01','2020-01-15 20:58:43',NULL),(5,'15120201638HJCMND08','Edgar Ramos','3311607633','Mustang ','Rojo','HJCMND08','1','450.0','SI','2020-01-15 16:03:08','2020-01-15 17:40:30',NULL),(6,'151202020321899##','No_name','No_phone','null','null','99##','1','100','C','2020-01-15 20:32:18','2020-01-15 21:35:46',NULL),(7,'161202075111Fulano1234','Fulano Perez','','','','Fulano1234','1','450.0','NO','2020-01-16 07:51:11','0000-00-00 00:00:00',NULL),(8,'16120207535Jurado1234','Juan Jurado','','','','Jurado1234','1','152.0','NO','2020-01-16 07:53:05','0000-00-00 00:00:00',NULL),(9,'161202075454Elma1234','Elizabet María','','','','Elma1234','1','100','NO','2020-01-16 07:54:54','0000-00-00 00:00:00',NULL),(10,'161202075516GilA1234','Gilberto Aceves','','','','GilA1234','0','15','NO','2020-01-16 07:55:16','0000-00-00 00:00:00',NULL),(11,'161202075545Jupe1234','Juan Perez','','','','Jupe1234','0','15','NO','2020-01-16 07:55:45','0000-00-00 00:00:00',NULL),(12,'16120207560LuisA1234','Luis Aleberto','','','','LuisA1234','0','15','NO','2020-01-16 07:56:00','0000-00-00 00:00:00',NULL),(13,'161202075614Alemo1234','Alejandra Moreno','','','','Alemo1234','0','15','NO','2020-01-16 07:56:14','0000-00-00 00:00:00',NULL),(14,'161202075637Dama1234','Damian Marley','','','','Dama1234','0','15','NO','2020-01-16 07:56:37','0000-00-00 00:00:00',NULL),(15,'161202075656Julpe1234','Julia Perez','','','','Julpe1234','0','15','NO','2020-01-16 07:56:56','0000-00-00 00:00:00',NULL),(16,'161202075713DaMon1234','Danira Monclova','','','','DaMon1234','0','15','NO','2020-01-16 07:57:13','0000-00-00 00:00:00',NULL),(17,'161202075729Juame1234','Juana Medina','','','','Juame1234','0','15','NO','2020-01-16 07:57:29','0000-00-00 00:00:00',NULL),(18,'161202075750Raul1234','Raul Perez','','','','Raul1234','0','15','NO','2020-01-16 07:57:50','0000-00-00 00:00:00',NULL),(19,'161202075832Palacios1234','Raul Palacios','','','','Palacios1234','0','12.5','NO','2020-01-16 07:58:32','0000-00-00 00:00:00',NULL),(20,'16120207592LuisAl1234','Luisa Alvarez','','','','LuisAl1234','1','100','NO','2020-01-16 07:59:02','0000-00-00 00:00:00',NULL),(21,'161202075930LeoBa1234','Leonarda Genoveba','','','','LeoBa1234','1','100','NO','2020-01-16 07:59:30','0000-00-00 00:00:00',NULL),(22,'16120208035RaRa1234','Ramira Ramona','','','','RaRa1234','0','12.5','NO','2020-01-16 08:00:35','0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `tbl_esta_reg` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-26 19:51:46
