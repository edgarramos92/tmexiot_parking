#Este archivo se encarga de realizar las tareas de los botones de "Buscar" y de "Pagar".

#>>>>>>>>>>>>>>>>>>>>> Paqueteria importada <<<<<<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

import fileinput
import numpy as np#instalable
#from Tkinter import *#instalable
from tkinter import ttk#instalable
import mysql.connector
from tkinter import *
from tkinter.scrolledtext import ScrolledText
import datetime
from datetime import datetime
from math import *
import os 
#from ScrolledText import * #instalable
from tkcalendar import DateEntry #instalable
from proy1_8 import stringchk


#>>>>>>>>>>>>>>>>>>> Botón para buscar clientes <<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
#Esta función corresponde al botón "Buscar" , se utiliza para encontrar al cliente y poder realizar el pago

def find(marco,e,lab,entr, cursor, conn):
	"""Función para buscar por CB/Placa del coche"""
	global chk, cb, name, costotal, ID
	varstr = stringchk(e)
	if e[0][1].get() != '':
		if varstr:
			b = e[0][1].get()
			#print(e)
			cb  = b
			val = (b,b)
			cursor.execute("SELECT  reg_name, reg_phone, reg_model, reg_color, reg_placa, reg_checkin, reg_pension, reg_costo, reg_CB, reg_pagado, reg_id FROM tbl_esta_reg WHERE ((reg_CB = %s  OR reg_placa = %s) AND reg_pagado = 'NO')  ORDER BY reg_checkin DESC",val)
			res = cursor.fetchall()
			print(res)
			if res != []:
				ID = res[0][10]
				print(ID)
				#with open ("data.csv", "r") as archivo:
				#	dat = archivo.readlines()
				#dat = [x.strip() for x in dat]
				#costo = float(dat[1])
		
				#with open ("Lista.csv", "r") as archivo:
				#	model = archivo.readlines()
				#model = [x.strip() for x in model]
				#i = 3
				#r = 0
				#print np.size(e)
				#print (e)

			
				print('res',(res))
				name = res[0][1]
			
				#print('imprimiendo en pantalla todos las entradascon las caracteristicas dadas', np.shape(res)[0])
				index = 1
				if np.size(res) > 0:
					now = datetime.now()
					index = 0
				elif np.size(res) == 0: 
					index = 1
			
				if index == 0:
					print(res)
					time = now - res[0][5] 
					print(time)
					if int(res[0][6]) == 0:
						for en,i in zip(entr,range(np.size(res))):
							en[1].delete(0,END)
							en[1].insert(0,res[0][i])
							costotal = ceil(time.days*24+time.seconds/3600)*float(res[0][7])
							Label(marco,  text=' Fecha de ingreso %s \nFecha de salida %s \nTiempo total %.2f (horas) \n Costo al momento de ingresar: $%s  Costo total: $%.2f'%(res[0][5],now,ceil( time.days*24+time.seconds/3600), res[0][7], ceil(time.days*24+time.seconds/3600)*float(res[0][7])), anchor='w').grid(padx=5, pady=5, row=5, column=2,rowspan = 2,sticky = (W))
							chk = 1
					if int(res[0][6]) == 1:
						for en,i in zip(entr,range(np.size(res))):
							en[1].delete(0,END)
							en[1].insert(0,res[0][i])
							costotal = ceil(time.days+time.seconds/(3600*24))*float(res[0][7])
							Label(marco,  text=' Fecha de ingreso %s \nFecha de salida %s \nTiempo total %i (dias) \nCosto al momento de ingresar: $%s  Costo total: $%.2f'%(res[0][5], now, ceil(time.days+time.seconds/(3600*24)), res[0][7], ceil(time.days+time.seconds/(3600*24))*float(res[0][7])), anchor='w').grid(padx=5, pady=5, row=5, column=2,rowspan = 2,sticky = (W))
							chk = 1
					status = Label(marco, text='Se encontró su busqueda. Fecha de ingreso: %s '%res[0][5], bd=1, relief=SUNKEN)
		
					status.grid(row=12, column=0, columnspan=4, rowspan=4)
					status.config(width=82) 
					print("chk de find",chk)
			else:
				chk = 0
				status = Label(marco, text='No se encontró su busqueda, vuelva a intentarlo', bd=1, relief=SUNKEN)

				status.grid(row=12, column=0, columnspan=4, rowspan=4)
				status.config(width=82) 	
					#time ='%s,%s,%s,%s,%s,%s '%( now.day,now.month,now.year,now.hour,now.minute,now.second)
					#for entry in e:
					#	b = entry[1].get()
		
					#print model
					#print np.shape(model)
		
					#while i < np.size(model):
						#print model[i]
						#c =  model[i].split('\t\t')
						#print np.size(c),i,i+i
						#print c
						#print b
						#v = i % 2
						#print c,i
						#print c[5]
				#	  print b

						#if (c[0] == b or c[5] == b):
						#	if c[9] == 'NO':
					
								#print ('Si existe su busqueda, se encuentra en la línea :',i,'\n')
						 #print model[0]+'\n'
						 #print model[i]+'\n'
						#index=i
					
						#a = [str(r) for r in c[6].split(',')]#de una lista de  
						 #print c[7],c[8],c[9]
				 		 #if j == 0:
						 #	a = [str(r) for r in c[j-1].split(',')]#de una lista de numeros string separados por comas a un vetor hecho por estos numeors
						#b = [int(r) for r in time.split(',')]
					 
						 #print 'Fecha de ingreso : %s/ %s/ %s %s:%s:%s Fecha de salida %i/ %i/ %i %i:%i:%i'%(a[0],a[1],a[2],a[3],a[4],a[5],b[0],b[1],b[2],b[3],b[4],b[5])
						#for i,j in zip(a,range(np.size(a))):
					
									#a[j] = int(a[j])
						#duracion = (b[0]*24*60+b[1]*30*24*60+b[2]*365*30*24*60+b[3]*60+b[4] - (a[0]*24*60+a[1]*30*24*60+a[2]*365*30*24*60+a[3]*60+a[4]))/60.
					 #print 'Tiempo de duracion : %.2f horas (%i minutos)'%(duracion,duracion*60)
							#print 'Costo total : $%.2f'%(ceil(duracion)*float(c[8]))
					
							
					#else:
				
				 		 #print c[10]
						#status = Label(root, text='Fecha de ingreso: %s/%s/%s %s:%s:%s  Fecha de Salida  %s/%s/%s %s:%s:%s'%(c[6].split(',')[0],c[6].split(',')[1],c[6].split(',')[2],c[6].split(',')[3],c[6].split(',')[4],c[6].split(',')[5],c[10].split(',')[0],c[10].split(',')[1],c[10].split(',')[2],c[10].split(',')[3],c[10].split(',')[4],c[10].split(',')[5]), bd=1, relief=SUNKEN)

						#status.grid(row=12,column=0,columnspan=4,rowspan=4)
						#status.config(width=82)
			
						#if r == np.size(model)-3 or r == np.size(model):
						#	status = Label(root, text='No se encontró su busqueda, vuelva a intentarlo', bd=1, relief=SUNKEN)

						#	status.grid(row=12,column=0,columnspan=4,rowspan=4)
						#	status.config(width=82) 	

						
						#i = i + 1 
				#	print np.size(model)
		else:
			status = Label(marco, text='Consulta la lista de caracteres no admitidos en el menú Ayuda.', bd=1, relief=SUNKEN)

			status.grid(row=12, column=0, columnspan=4, rowspan=4)
			status.config(width=82) 	
	else:
		chk = 0
		status = Label(marco, text='No se encontró su busqueda, vuelva a intentarlo', bd=1, relief=SUNKEN)

		status.grid(row=12,column=0,columnspan=4,rowspan=4)
		status.config(width=82) 	
	

#>>>>>>>>>>>>>>>>>> Botón de para realizar el pago <<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>


#Esta funcion corresponde al boton de pago
def pay(marco,lab, cursor,conn ):
	global chk
	#with open('Lista.csv','r') as read:
	#	dat1 = read.readlines()
	#dat1 =  [x.strip() for x in dat1]
	try:
		if chk == 1 :


			val2 =("SI", datetime.now(), costotal,ID)

			cursor.execute("UPDATE tbl_esta_reg SET reg_pagado = %s ,reg_checkout = %s, reg_costoT = %s WHERE reg_id = %s ",val2)

			conn.commit()
			print(cursor.rowcount, "record(s) affected")
			#da =  dat1[index].split('\t\t')
			#print (da[10],'jajaja')
			#if da[10] =='no':
			#	now = datetime.now()
			status = Label(marco, text='Gracias por su pago', bd=1, relief=SUNKEN)

			status.grid(row=12,column=0,columnspan=4,rowspan=4)
			status.config(width=82)
			chk = 0
			#	i = 0 
			#	dat1[index] = dat1[index].replace('\t\tNO','\t\tSI')
			#	dat1[index] = dat1[index].replace('\t\tno','\t\t'+str(now.day)+','+str(now.month)+','+str(now.year)+','+str(now.hour)+','+str(now.minute)+','+str(now.second))
			#	print (dat1[index])
			#	f = open('Lista.csv','w')
			#	for i in dat1:
			#		f.write(i+'\n')
			#else:
			#	status = Label(marco, text='El coche salió el día: '+str('/'.join(da[10].split(',')))+', vuelva a intentarlo con otro CB.', bd=1, relief=SUNKEN)

	#			status.grid(row=12,column=0,columnspan=4,rowspan=4)
	#			status.config(width=82)
	#		dat2 = dat1[index].split('\t\t')  
	#		dat2.replace('\t\tNO\t\t','\t\tSI\t\t')
	#		print(line.replace(text_to_search, replacement_text), end='')

		 
		else :
			status = Label(marco, text='Por favor primero busque el CB o placa del coche.', bd=1, relief=SUNKEN)
			chk = 0
			status.grid(row=12, column=0, columnspan=4, rowspan=4)
			status.config(width=82) 	
	except:
		status = Label(marco, text='Por favor primero busque el CB o placa del coche.', bd=1, relief=SUNKEN)
		chk = 0
		status.grid(row=12,column=0,columnspan=4,rowspan=4)
		status.config(width=82) 

#>>>>>>>>>>>>>>>>>> Botón de para condonar el pago <<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>


#Esta funcion corresponde al boton de pago
def cond(s, cursor,conn, marco ):
	ent = []
	print ("ora!", s[0].get(1.0, 'end-1c'), s[1].get())
	for i,j in zip(s,range(np.size(s))):
		
		if j == 0 :
			#print("text box", i.get(1.0, END), i)
			if i.get(1.0, 'end-1c') != "" and i.get(1.0, 'end-1c') != " " and i.get(1.0, 'end-1c') != "  ": 
				
				varint = s[1].get()
				if varint != "":
					#print("varinte type :",type(varint))
					#try:
					varint = float(varint)
					print("prueba1")
					val =("C", datetime.now(), varint, s[0].get(1.0, 'end-1c'), ID)
					cursor.execute("UPDATE tbl_esta_reg SET reg_pagado = %s, reg_checkout = %s, reg_costoT = %s,  reg_comment = %s WHERE reg_id = %s",val)
					conn.commit()
					print("ora ora :v :v")
					status = Label(marco, text='Comentario guardado, costo: $%s'%str(varint), bd=1, relief=SUNKEN)
					status.grid(row=12, column=0, columnspan=4, rowspan=4)
					status.config(width=62)
					
				elif varint == "":
					print("prueba2")
					val =("C", datetime.now(), 0, s[0].get(1.0, 'end-1c'), ID)
					cursor.execute("UPDATE tbl_esta_reg SET reg_pagado = %s, reg_checkout = %s, reg_costoT = %s, reg_comment = %s WHERE reg_id = %s",val)
					conn.commit()
					status = Label(marco, text='Comentario guardado, costo: $%s'%("0"), bd=1, relief=SUNKEN)
					status.grid(row=12, column=0, columnspan=4, rowspan=4)
					status.config(width=62)
				else:
					print("prueba3")
				
				
			else :
				#print("text box 2",i.get(1.0, END))
				#ent.append(i.get(1.0, END))
				status = Label(marco, text='Es necesario que escribas el motivo.', bd=1, relief=SUNKEN)
				status.grid(row=12, column=0, columnspan=4, rowspan=4)
				status.config(width=62)
				
				
				
				
				
		elif j == 1:


			print(i.get())










#>>>>>>>>>>>>>>>>>> Botón de para condonar el pago <<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>


#Esta funcion corresponde al boton de condonar
def condo(root,lab, cursor,conn ):
	global chk
	#with open('Lista.csv','r') as read:
	#	dat1 = read.readlines()
	#dat1 =  [x.strip() for x in dat1]
	
	try:
		print (chk)
		if chk == 1 :

			top = Toplevel(root)
#			top.geometry("820x395")
			top.title('Hacer checkout')
			marco = top
			
			
			Label(marco, text = "A continuación agrega el motivo de hacer checkout por este medio: ").grid(row=0, column=0, columnspan=4, rowspan=1, sticky = (W))
			entries = []
			textBox = ScrolledText(top,width=45, height=10, borderwidth=3, relief="sunken")
			textBox.grid(padx=5, pady=5, row=1, column=0,rowspan = 2, columnspan = 2,  sticky = (W))
			
			entries.append(textBox)		
			
			Label(marco, text = "Cambiar costo: ").grid(row=5, column=0, sticky = (W))
			ent = Entry(marco, width=15)
			ent.grid(padx=5, pady=5, row=5, column=1, sticky = ( W))
			
			entries.append(ent)
			
			B1 = Button(marco, width=8, text='Aceptar', command=(lambda s = entries: cond(s, cursor, conn, marco))).grid(padx=5, pady=5, row=6, column=0, sticky = (N, S, E, W))
			B2 = Button(marco, width=8, text='Cancelar', command=marco.destroy).grid(padx=5, pady=5, row=6, column=1, sticky = (N, S, E, W))
			
			
			
			
			#val2 =("C", datetime.now(),cb,cb)
			#cursor.execute("UPDATE tbl_esta_reg SET reg_pagado = %s ,reg_checkout = %s WHERE reg_CB = %s or reg_placa = %s",val2)

			#conn.commit()
			#print(cursor.rowcount, "record(s) affected")
			#da =  dat1[index].split('\t\t')
			#print (da[10],'jajaja')
			#if da[10] =='no':
			#	now = datetime.now()
			status = Label(marco, text="Agrega un comentario para hacer checkout", bd=1, relief=SUNKEN)

			status.grid(row=12, column=0, columnspan=4, rowspan=4)
			status.config(width=62)
			#chk = 0
			#	i = 0 
			#	dat1[index] = dat1[index].replace('\t\tNO','\t\tSI')
			#	dat1[index] = dat1[index].replace('\t\tno','\t\t'+str(now.day)+','+str(now.month)+','+str(now.year)+','+str(now.hour)+','+str(now.minute)+','+str(now.second))
			#	print (dat1[index])
			#	f = open('Lista.csv','w')
			#	for i in dat1:
			#		f.write(i+'\n')
			#else:
			#	status = Label(marco, text='El coche salió el día: '+str('/'.join(da[10].split(',')))+', vuelva a intentarlo con otro CB.', bd=1, relief=SUNKEN)

	#			status.grid(row=12,column=0,columnspan=4,rowspan=4)
	#			status.config(width=82)
	#		dat2 = dat1[index].split('\t\t')  
	#		dat2.replace('\t\tNO\t\t','\t\tSI\t\t')
	#		print(line.replace(text_to_search, replacement_text), end='')
		else:
			status = Label(marco, text="Esta función hace checkout a un cliente sin realizar el cobro. Primero busque un coche.", bd=1, relief=SUNKEN)
			chk = 0
			status.grid(row=12, column=0, columnspan=4, rowspan=4)
			status.config(width=82)


	except:	 
		status = Label(root, text="Esta función hace checkout a un cliente sin realizar el cobro. Primero busque un coche.", bd=1, relief=SUNKEN)
		chk = 0
		status.grid(row=12,column=0,columnspan=4,rowspan=4)
		status.config(width=82)





















