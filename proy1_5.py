#Este archivo se encarga de realizar las tareas del menú "convenios".

#>>>>>>>>>>>>>>>>>>>>> Paqueteria importada <<<<<<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

import fileinput
import numpy as np#instalable
#from Tkinter import *#instalable
from tkinter import ttk#instalable
import mysql.connector
from tkinter import *
from tkinter.scrolledtext import ScrolledText
import datetime
from datetime import datetime
from math import *
import os, sys
#from ScrolledText import * #instalable
from tkcalendar import DateEntry #instalable
from werkzeug import generate_password_hash, check_password_hash
from proy1_8 import stringchk4
from proy1_8 import stringchk3


#>>>>>>>>>>>>>>>>>>> Menú convenios <<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
#Esta función corresponde al menú "Convenios". Este despliega una ventana para editar los convenios, agregar y borrar convenios y visuaalizar un historial de todos los convenios, 

#$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$$%$%$%$
#aqui se abre la ventana de los convenios

def windowConv(root, cursor, conn, fields4):
	def hist(	):
		top = Toplevel(root)
		top.title('Historial de Convenios')
		status = Label(top, text='Aquí se muestra el historial de convenios.', bd=1, relief=SUNKEN)
		status.grid(row=5,column=0, columnspan=4,rowspan=4)
		status.config(width=80)
		textBox = ScrolledText(top,width=80, height=10, borderwidth=3, relief="sunken")

		textBox.grid(padx=5, pady=5, row=0, column=0,rowspan = 2, columnspan = 2,  sticky = (W))
		tbl = "Nombre    \t\t Costo  \t\t  Fecha de registro   \t\t  Vigencia \n" 
		
		cursor.execute("SELECT conv_id, conv_name, conv_costo, conv_tipo, conv_note, conv_fecha, conv_vigencia, conv_nvigencia  FROM tbl_esta_conv")
		dat = cursor.fetchall()
		print('dat :v :v ',dat)
		tbl1 = []
		for i in dat:
			if i[6] == 1:
				tbl1.append(i[1]+' \t\t $'+str(i[2])+'\t\t    '+str(i[5])+'\t\t           Vigente \n' )
			else:
				tbl1.append(i[1]+' \t\t $'+str(i[2])+'\t\t    '+str(i[5])+'\t\t           '+str(i[7])+'\n' )	
		for i in tbl1:
			textBox.insert('1.0',i)
		#textBox.insert('0.0',tbl1[1])
		textBox.insert('1.0',tbl)
		textBox = ScrolledText(top,width=80, height=10, borderwidth=3, relief="sunken", state = 'disabled')
	def delC(ent,tBx,dat,var,conv_id,varint):
		#try:
			#print("dat ", dat)
			#print("var ", var) 
		b1 = Button(marco, width=6, text='Borrar', state='disabled').grid(padx=5, pady=5, row=5, column=2, sticky = (N, S, E, W))
		now1 = datetime.today()
		val = (0, conv_id)
		print(":v",0, conv_id)
		cursor.execute("UPDATE tbl_esta_conv SET conv_vigencia = %s , conv_nvigencia = curdate() WHERE conv_id = %s ",val)

		conn.commit()
		status = Label(marco, text="Convenio eliminado exitosamente, reinicie ventana", bd=1, relief=SUNKEN)
		status.grid(row=12, column=0, columnspan=4, rowspan=4)
		status.config(width=82)
		"""except:
			status = Label(marco, text="Elija otro convenio", bd=1, relief=SUNKEN)
			status.grid(row=12, column=0, columnspan=4, rowspan=4)
			status.config(width=82)"""
			#tBx.delete('1.0',END)
			#for i in ent:
			#	i.delete(0,END)
			#f = open('convenios.csv','w')
			#l = open('convenios.csv','a')
			#for i in dat1:
			#	  f.write(i+'\n')
			#for i,j in zip(dat,range(np.size(dat))):
			#	if i == var.get():
			#		print (str(i)+' = '+str(var.get())+' en la posicion : '+str(j))
			#		indice = 1
			#	if i!= var.get():
			#		l.write(i+'\n')

#___________________________________________________________________

	def SaveC(ent,tBx,var1):
		var = []
		#print(var1.get())
		#print("entrie ",entrie)
		#print("entrie2",entrie2)
		ent2 = []
		#ent2.append(ent)
		#ent2.append(ent)
		#print("ent ",ent2)
		varstr = stringchk3(entrie)
		varstr2 = stringchk4(entrie2)
		index = 0
		for i,j in zip(ent,range(np.size(ent))):
			if i.get() != '':
				index = 1
				var.append(i.get())
				print("i :", i.get())
			
			else:
				status = Label(marco, text='Es necesario el nombre y costo por convenio.', bd=1, relief=SUNKEN)

				status.grid(row=12,column=0, columnspan=4,rowspan=4)
				status.config(width=82)
				index = 0
		print(index)
		print(var)
		if varstr and varstr2:
			if index == 1:
				print(index)
				#var.append(tBx.get(1.0, END))
				#sav = '%s\t\t%s\t\t%s\t\t%s\n'%(var[0],var[1],var[2],var1.get())
				#l=open ("convenios.csv", 'a')
				#l.write(sav)
				now = datetime.today()
				print(now)
				val = (var[0], var[1], tBx.get(1.0, END), now, var1.get())
				cursor.execute("INSERT INTO tbl_esta_conv (conv_name,conv_costo,conv_note,conv_fecha,conv_tipo ) VALUES (%s, %s, %s, %s, %s)", val)
				conn.commit()
				status = Label(marco, text='Convenio guardado con éxito: Convenio: %s  Costo: %s'%(var[0],var[1]), bd=1, relief=SUNKEN)
				status.grid(row=12,column=0,columnspan=4,rowspan=4)
				status.config(width=82)
			else:
				status = Label(marco, text='Es necesario el nombre y costo por convenio. ', bd=1, relief=SUNKEN)

				status.grid(row=12,column=0, columnspan=4,rowspan=4)
				status.config(width=82)
				index = 0

		else:
			status = Label(marco, text="Consulta la lista de caracteres no admitidos en el menú Ayuda.", bd=1, relief=SUNKEN)
			status.grid(row=12,column=0,columnspan=4,rowspan=4)
			status.config(width=82)
#___________________________________________________________________

	def callback(dat,var,ent,tBx,status,b1):
		print ('dat',(dat))
		

		#status = Label(marco, text="Nombre del convenio: %s Fecha: "%(), bd=1, relief=SUNKEN)
		varint = int((var.get()).split(" ")[0])
		
		print("dat", dat)
		print("varint ", varint)
		#print("dat[varint-1][4] ", dat[varint-1][4])		
		for i in dat:
			print(i[0])
			if int(i[0]) == varint:
				dat2 = i[4]

		tBx.delete('1.0',END)
		try:
			tBx.insert('1.0',dat2)
		except:
			tBx.insert('1.0','')
		#print ent
		#print(ent)
		#"""for e,e1 in zip(ent,range(np.size(ent))):
		#	e.delete(0,END)
		#	e.insert(0,dat[varint-1][e1+1])
		#	if dat[varint-1][3] == 0:
		#		Checkbutton(marco, text="Hora", onvalue = 0, variable=var1).grid(row=2,column = 1, sticky=W)
		#		Checkbutton(marco, text="Día", onvalue = 1, variable=var1).grid(row=2,column = 1, sticky=W)
		#	if dat[varint-1][3] == 1:
		#		Checkbutton(marco, text="Día", onvalue = 0, variable=var1).grid(row=2,column = 1, sticky=W)
		#		Checkbutton(marco, text="Hora", onvalue = 1, variable=var1).grid(row=2,column = 1, sticky=W)
		#	print("texto :v",dat[varint-1][e1+1])"""
		#	print("textoo ",(var.get().split('\t\t'))[e1])
		print("dat: ",dat)
		for i in dat:
			if i[0] == varint:
				conv = "Nombre del Convenio: %s\nCosto: $%s          \nFecha de Registro: %s "%(i[1], i[2], i[5])
				conv_id = i[0]
				Label(marco,  text=conv, anchor='w').grid(padx=5, pady=5, row=2, column=2, rowspan = 2, columnspan = 2, sticky = (W))
				b1 = Button(marco, width=6, text='Borrar', command=(lambda :(delC(ent, tBx, dat, var, conv_id, varint))), state = 'normal' ).grid(padx=5, pady=5, row=5, column=2, sticky = (N, S, E, W)) 
		 
		
#___________________________________________________________________

#Aqui se crea la ventana, se le asigna un titulo y se agrega la status bar	 
	top = Toplevel(root)
	top.title('Administrador de Convenios')
	marco = top
	status = Label(marco, text="Bienvenido al Administrador de convenios", bd=1, relief=SUNKEN)
	status.grid(row=12, column=0, columnspan=4, rowspan=4)
	status.config(width=82)
	#with open ("convenios.csv", "r") as archivo:
	#	dat = archivo.readlines()
	#dat = [x.strip() for x in dat]
	
#Aquí se busca en la base de datos los convenios para luego desplegarlos en un combobox
	cursor.execute("SELECT conv_id, conv_name, conv_costo, conv_tipo, conv_note, conv_fecha FROM tbl_esta_conv  WHERE  conv_vigencia = 1")
	dat = cursor.fetchall()
	print("dat : ",dat)
	dat1 = []
	for i in dat:
		if i[2] == 1:
			dat1.append((str(i[0])+' '+str(i[1])+' costo: '+str(i[2])+'/hora' ))
		else:
			dat1.append((str(i[0])+' '+str(i[1])+' costo: '+str(i[2])+'/día' ))
	print("dat1 : ",dat1)
	entries = []	
	entrie = []
	i = 0
	entrie2 = []
	while i < np.size(fields4):
		if i <= 4:
			if i == 2:
				Label(top, width=10, text=fields4[i], anchor='w').grid(padx=5, pady=5, row=i, column=0, sticky = (N, S, E, W))
				textBox = ScrolledText(top,width=45, height=10, borderwidth=3, relief="sunken")

				textBox.grid(padx=5, pady=5, row=3, column=0,rowspan = 2, columnspan = 2,  sticky = (W))

			else:
				Label(marco, width=10, text=fields4[i], anchor='w').grid(padx=5, pady=5, row=i, column=0, sticky = (N, S, E, W))
				ent = Entry(marco, width=15)
				ent.grid(padx=5, pady=5, row=i, column=1, sticky = (N, S, E, W))
				entries.append(ent)
				entrie.append((ent,ent))
	  
		i = i + 1
	entrie2.append((textBox,textBox))
	entrie2.append((textBox,textBox))
	var1 = IntVar()
	print(dat1)
	conv = "Administrador de convenios\n Puede utilizar esta herramienta para agregar\n o eliminar un convenio."
	Label(marco,  text=conv, anchor='e').grid(padx=5, pady=5, row=0, column=2, rowspan = 2, columnspan = 2, sticky = (E))
	
	Checkbutton(marco, text="Hora", onvalue = 0, variable=var1).grid(row=2,column = 1, sticky=W)
	Checkbutton(marco, text="Día", onvalue = 1, variable=var1).grid(row=2,column = 1, sticky=E)
	b1 = Button(marco, width=6, text='Borrar', command=(lambda s = entries: delC()), state='disabled').grid(padx=5, pady=5, row=5, column=2, sticky = (N, S, E, W)) 
	b2 = Button(marco, width=6, text='Guardar',command=(lambda : SaveC(entries,textBox, var1))).grid(padx=5, pady=5, row=5, column=1,  sticky = (N, S, E, W))
	var = StringVar()
	Co =  ttk.Combobox(marco,width=6,values = dat1, textvariable=var )
	Co.grid(padx=5, pady=5, row=5, column=0, sticky = (N, S, E, W))
	
	Co.bind('<<ComboboxSelected>>', lambda event: callback(dat,var,entries,textBox,status,b1))
	b4 = Button(marco, width=8, text='Salir', command=marco.destroy).grid(padx=5, pady=5, row=5, column=3, sticky = (N, S, E, W))
	b5 = Button(marco, width=6, text='Histórico', command=(lambda :(hist()))).grid(padx=5, pady=5, row=4, column=2, sticky = (N, S, E, W))
	#b6 = Button(marco, width=6, text='Histórico', command=(lambda :(hist()))).grid(padx=5, pady=5, row=4, column=3, sticky = (N, S, E, W))
	status = Label(marco, text="Bienvenido al Administrador de convenios", bd=1, relief=SUNKEN)
	status.grid(row=12, column=0, columnspan=4, rowspan=4)
	status.config(width=82)

