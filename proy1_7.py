#Este archivo se encarga de realizar las tareas del menú "Busqueda".

#>>>>>>>>>>>>>>>>>>>>> Paqueteria importada <<<<<<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

import fileinput
import numpy as np#instalable
#from Tkinter import *#instalable
from tkinter import ttk#instalable
import mysql.connector
from tkinter import *
from tkinter.scrolledtext import ScrolledText
import datetime
from datetime import datetime
from datetime import timedelta
from datetime import date
from math import *
import os, sys
#from ScrolledText import * #instalable
from tkcalendar import DateEntry #instalable
from werkzeug import generate_password_hash, check_password_hash
from proy1_8 import stringchk5


#>>>>>>>>>>>>>>>>>>> Menú Busqueda <<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
#Esta función corresponde al menú "Busqueda". Este despliega una ventana para buscar clientes dentro del sistema. 

#$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$%$$%$%$%$


def busqueda(root, conn, cursor):
	global tab1, tab2
	entries = []
	s = 0
	#window = Tk()
	top = Toplevel(root)
#	top.geometry("820x395")
	top.title('Busqueda')
	tab_parent = ttk.Notebook(top)
	tab1 = ttk.Frame(tab_parent)
	tab2 = ttk.Frame(tab_parent)
	tab_parent.add(tab1, text = "Por fecha")
	tab_parent.add(tab2, text = "Por campo")
	tab_parent.grid(row = 0, column = 0)
	
#Aquí se genera el contenido del tab 1	
	
	Label(tab1,  text="Fecha inicial").grid(padx=5, pady=5, row=0, column=0, sticky = (W))
	ent = DateEntry(tab1, width=10)
	ent.grid(padx=5, pady=5, row=0, column=0, sticky = (E))
	entries.append(ent)
	Label(tab1,  text="Fecha final").grid(padx=5, pady=5, row=0, column=1, sticky = (W))
	ent = DateEntry(tab1, width=10)
	ent.grid(padx=5, pady=5, row=0, column=1, sticky = (E))
	entries.append(ent)
	textBox1 = ScrolledText(tab1,width=140, height=10, borderwidth=3, relief="sunken")
	textBox1.bind("<Key>", lambda e: "break")
	textBox1.grid(padx=5, pady=5, row=3, column=0,rowspan = 2, columnspan = 4,  sticky = (W))
	text1 = 'CB\t\tNombre\t\tModelo\t\tPlaca\t  Costo\t\tFecha ingreso\t\tFecha salida\t  Total \t Comentario \t '
	Label(tab1,  text=text1).grid(padx=5, pady=5, row=2, column=0,columnspan = 3, sticky =  (W))	
		#entries.append((i, ent))
	tex = ' Coches adentro:\nCoches afuera:'
	lab = Label(tab1,  text=tex, font=(16)).grid(padx=5, pady=5, row=6, column=0, rowspan = 2,  sticky =  (W)) 
	tex = ' Total por cobrar:\nTotal recaudado:'
	lab = Label(tab1,  text=tex, font=(16)).grid(padx=5, pady=5, row=6, column=1, rowspan = 2,  sticky =  (W)) 
	b1 = Button(tab1, width=8, text='Buscar',command=(lambda s = entries: find1(s,textBox1,tab1,lab, conn, cursor))).grid(padx=5, pady=5, row=8, column=0, sticky = (N, S, E, W) )
	b2 = Button(tab1, width=8, text='Salir', command=top.destroy).grid(padx=5, pady=5, row=8, column=1, sticky = (N, S, E, W))
	status = Label(tab1, text='Busqueda por fecha', bd=1, relief=SUNKEN)

	status.grid(row=12,column=0,columnspan=4)
	status.config(width=110) 
	
#Aquí se genera el contenido del tab 2
	
	var_ = StringVar()
	val_ = "Código de barras","Nombre","Placa","Modelo"
	Label(tab2,  text="Selecciona un campo:").grid(padx=5, pady=5, row=0, column=0, sticky = (W))
	b1 =  ttk.Combobox(tab2,width=18,values = val_, textvariable=var_ )
	b1.grid(padx=5, pady=5, row=0, column=0, sticky = (E))
	
	Label(tab2,  text="Ingrese su búsqueda").grid(padx=5, pady=5, row=0, column=1, sticky = (W))
	ent = Entry(tab2, width=12)
	ent.grid(padx=5, pady=5, row=0, column=1, sticky = (E))
	#entries.append(ent)
	textBox2 = ScrolledText(tab2,width=140, height=10, borderwidth=3, relief="sunken")
	textBox2.bind("<Key>", lambda e: "break")
	textBox2.grid(padx=5, pady=5, row=3, column=0,rowspan = 2, columnspan = 4,  sticky = (W))
	text = 'CB\t\tNombre\t\tModelo\t\tPlaca\t  Costo\t\tFecha ingreso\t\tFecha salida\t  Total \t Comentario \t'
	Label(tab2,  text=text).grid(padx=5, pady=5, row=2, column=0,columnspan = 3, sticky =  ( W))	
		#entries.append((i, ent))
	tex = ' Coches adentro:\nCoches afuera:'
	lab = Label(tab2,  text=tex, font=(16)).grid(padx=5, pady=5, row=6, column=0, rowspan = 2,  sticky =  (W)) 
	tex = ' Total por cobrar:\nTotal recaudado:'
	lab = Label(tab2,  text=tex, font=(16)).grid(padx=5, pady=5, row=6, column=1, rowspan = 2,  sticky =  (W)) 
	b1 = Button(tab2, width=8, text='Buscar', command=(lambda s = ent: find2(s, textBox2, tab2, lab, conn, cursor, val_, var_))).grid(padx=5, pady=5, row=8, column=0, sticky = (N, S, E, W) )
	b2 = Button(tab2, width=8, text='Salir', command=top.destroy).grid(padx=5, pady=5, row=8, column=1, sticky = (N, S, E, W))
	status = Label(tab2, text='Busqueda por Campo', bd=1, relief=SUNKEN)

	status.grid(row=12,column=0,columnspan=4)
	status.config(width=140) 




#def to_date(p,i):
#	if i == 0:
#		year, month, day = map(int, p.split(','))
#		print month, day, year,' aqui se imprimio'
#		return datetime(year, month, day, 0, 0, 0)
#	if i == 1:
#		day, month, year = map(int, p.split(','))
#		print month, day, year,' aqui se imprimio'
#		return datetime(year, month, day, 0, 0, 0)
#___________________________________________________________________
#>>>>>>>>>>>>>>>>>> Botón para Buscar por fecha <<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>


def find1(s,txbx,window,lab, conn, cursor):
	
	VAR = 0
	#with open ("Lista.csv", "r") as archivo:
	#	lista = archivo.readlines()
	#lista = [x.strip() for x in lista]
	b = []
	#print(s)
	dict_ = dict()
	for entry in s:
		b.append(entry.get_date())
	print("b = ", b)
	b2 =  b[1] + timedelta(days=1) 
	print("b2 = ", b2) 
	val = (b[0], b2)
	cursor.execute("SELECT * FROM tbl_esta_reg WHERE  reg_checkin >= %s and reg_checkin <= %s", val)
	res = cursor.fetchall()
	#print('res = ',res,'np.size ', np.shape(res))
	datos_dict = []
	datos_dict2 = []
	txbx.delete('1.0','end')
	duracion3 = 0
	duracion4 = 0
	cost = 0
	coches1 = 0
	coches2 = 0
	for i in res:
		#print (i)
		"""dict_["id"] = i[0]
		dict_["cb"] = i[1]
		dict_["Nombre"] = i[2]
		dict_["telefono"] = i[3]
		dict_["Modelo"] = i[4]
		dict_["color"] = i[5]
		dict_["placa"] = i[6]
		dict_["pension"] = i[7]
		dict_["costo"] = i[8]
		dict_["pagado"] = i[9]
		dict_["in"] = i[10]
		dict_["out"] = i[11]
		dict_["coment"] = i[12]
		dict_["costoT"] = i[13]
		datos_dict.append(dict_)"""

		if i[9] == "NO":
			
			if i[7] == '0' or i[7] == 0:
				print("hora adentro", i[8])
				dur1 = datetime.now()-i[10]
				cost1 = ceil(int(dur1.days)*24 + int(dur1.seconds)/3600)*float(i[8])
				cost = cost + cost1
				coches1 = coches1 + 1
				print(cost1) 
				print(dur1)
				txbx.insert("1.0", (i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora\t\t" + str(i[10]) + "\t\t---------NA---------\t\t "+str(cost1)+" \t Sin comentario\n"),"d1")
				txbx.tag_config('d1', foreground='blue')  

				
				
			if i[7] == '1' or i[7] == 1:
				print("pension adentro", i[8])
				dur1 = datetime.now()-i[10]
				dur2 =   datetime.now()-i[10]
				cost2 = ceil(int(dur1.days) + int(dur1.seconds)/(3600*24))*float(i[8])
				cost = cost + cost2
				coches1 = coches1 + 1
				print(cost2)
				print(dur2)
				txbx.insert("1.0",( i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día\t\t" + str(i[10]) + "\t\t----------NA----------\t\t "+str(cost2)+" \t Sin comentario\n"), "d2")
				txbx.tag_config('d2', foreground='gray')
				
		else:
			
			if i[7] == '0' or i[7] == 0:
				coches2 = coches2 + 1
				print("hora afuera")
				#dur3 = i[11] - i[10]
				#print(dur3)
				if i[9] == "C":
					try:
						txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t "+i[12]+"\n")
					except:
						txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
					try:
						duracion3 = i[13] + duracion3
					except:
						duracion3 = duracion3
				elif i[9] == "SI":
					txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
					duracion3 = i[13] + duracion3
			if i[7] == '1' or i[7] == 1:
				coches2 = coches2 + 1
				print("pension afuera")
				#dur4 = i[11] - i[10]
				#print(dur4)
				if i[9] == "C":
					
					try:
						txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t "+i[12]+"\n")
					except:
						txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
					try:
						duracion3 = i[13] + duracion3
					except:
						duracion3 = duracion3
				elif i[9] == "SI":
					txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
					duracion3 = i[13] + duracion3
	
	#print(datos_dict2)		
	#print(datos_dict2)
	#txbx.insert("1.0", datos_dict2)
	#txbx.bind("<Enter>", enter)
	
	texto = ""	
		
		
		#'Nombre\t\tModelo\t\tPlaca\t\tCosto/pensiont\tFecha ingreso\t\tFecha salida'
	#i = 3
	#var = np.size(lista)-i
	#b1 = str(b[0]).split('-')
	#b2 = str(b[1]).split('-')
	#a1 = ','.join(b1)
	#a2 = ','.join(b2)
	#b1_ = to_date(a1,VAR)
	#b2_ = to_date(a2,VAR)
	
	duracion1 = []
	duracion2 = []
	
	#txbx.delete('1.0','end')
	
	print("coches adentro: ",coches1, " coches afuera: ", coches2, " costo por cobra: ", cost, " costo recaudado: ", duracion3)
	
	#x,y = Total(duracion1,duracion2,duracion3,duracion4)
	tex = '    %i\n    %i'%(coches1, coches2)
	Label(window,  text=tex, font=(16), fg="red").grid(padx=5, pady=5, row=6, column=0,columnspan = 1, rowspan = 2,  sticky =  (E)) 
	tex = '        $%.2f \n        $%.2f'%(cost, duracion3)
	Label(window,  text=tex, font=(16), fg="red").grid(padx=5, pady=5, row=6, column=1, rowspan = 2,  sticky =  (E)) 
	#text = '______________________________________________________________________________________________________________\n'
	#txbx.insert('1.0',text)
	#print 'duracion :',duracion1,np.shape(duracion3),np.shape(duracion2)[0],np.shape(duracion4)
	"""tex = 'Hay:\t %i coches en modo por hora adentro \n\t %i coches en modo pension adentro \nSalieron:\t %i coches en modo por hora \n\t %i coches en modo pensión'%(np.size(duracion1), np.size(duracion3), np.size(duracion2), np.size(duracion4))
	""" 
	

def find2(s,txbx,window,lab, conn, cursor, val_, var_):
	varstr = stringchk5(s)
	print(var_.get())
	#with open ("Lista.csv", "r") as archivo:
	#	lista = archivo.readlines()
	#lista = [x.strip() for x in lista]
	#print(s)
	dict_ = dict()
	b = s.get()
	if b != "" and b != " " and var_.get() != "":
	
		if varstr:

			#print("b = ", b)
			print(var_.get())
			if var_.get() == "Código de barras":
				val = (b,)
				cursor.execute("SELECT * FROM tbl_esta_reg WHERE  reg_CB = %s ", val)
				res = cursor.fetchall()
			elif var_.get() == "Nombre":
				val = ("%"+b+"%",)
				cursor.execute("SELECT * FROM tbl_esta_reg WHERE  reg_name LIKE %s ", val)
				res = cursor.fetchall()
			elif var_.get() == "Placa":
				val = (b,)
				cursor.execute("SELECT * FROM tbl_esta_reg WHERE  reg_placa = %s ", val)
				res = cursor.fetchall()
			elif var_.get() == "Modelo":
				val = ("%"+b+"%",)
				cursor.execute("SELECT * FROM tbl_esta_reg WHERE  reg_model LIKE %s ", val)
				res = cursor.fetchall()
			
			#print('res = ',res,'np.size ', np.shape(res))
			datos_dict = []
			datos_dict2 = []
			txbx.delete('1.0','end')
			duracion3 = 0
			duracion4 = 0
			cost = 0
			coches1 = 0
			coches2 = 0
			for i in res:
				#print (i)
				"""dict_["id"] = i[0]
				dict_["cb"] = i[1]
				dict_["Nombre"] = i[2]
				dict_["telefono"] = i[3]
				dict_["Modelo"] = i[4]
				dict_["color"] = i[5]
				dict_["placa"] = i[6]
				dict_["pension"] = i[7]
				dict_["costo"] = i[8]
				dict_["pagado"] = i[9]
				dict_["in"] = i[10]
				dict_["out"] = i[11]
				dict_["coment"] = i[12]
				dict_["costoT"] = i[13]
				datos_dict.append(dict_)"""

				if i[9] == "NO":
					
					if i[7] == '0' or i[7] == 0:
						print("hora adentro", i[8])
						dur1 = datetime.now()-i[10]
						cost1 = ceil(int(dur1.days)*24 + int(dur1.seconds)/3600)*float(i[8])
						cost = cost + cost1
						coches1 = coches1 + 1
						print(cost1) 
						print(dur1)
						txbx.insert("1.0", (i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora\t\t" + str(i[10]) + "\t\t---------NA---------\t\t "+str(cost1)+" \t Sin comentario\n"),"d1")
						txbx.tag_config('d1', foreground='blue')  

						
						
					if i[7] == '1' or i[7] == 1:
						print("pension adentro", i[8])
						dur2 =   datetime.now()-i[10]
						dur1 = datetime.now()-i[10]
						cost2 = ceil(int(dur1.days) + int(dur1.seconds)/(3600*24))*float(i[8])
						cost = cost + cost2
						coches1 = coches1 + 1
						print(cost2)
						print(dur2)
						txbx.insert("1.0",( i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día\t\t" + str(i[10]) + "\t\t----------NA----------\t\t "+str(cost2)+" \t Sin comentario\n"), "d2")
						txbx.tag_config('d2', foreground='gray')
						
				else:
					
					if i[7] == '0' or i[7] == 0:
						coches2 = coches2 + 1
						print("hora afuera")
						#dur3 = i[11] - i[10]
						#print(dur3)
						if i[9] == "C":
							try:
								txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t "+i[12]+"\n")
							except:
								txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
							try:
								duracion3 = i[13] + duracion3
							except:
								duracion3 = duracion3
						elif i[9] == "SI":
							txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/hora \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
							duracion3 = i[13] + duracion3
					if i[7] == '1' or i[7] == 1:
						coches2 = coches2 + 1
						print("pension afuera")
						#dur4 = i[11] - i[10]
						#print(dur4)
						if i[9] == "C":
							
							try:
								txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t "+i[12]+"\n")
							except:
								txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
							try:
								duracion3 = i[13] + duracion3
							except:
								duracion3 = duracion3
						elif i[9] == "SI":
							txbx.insert("1.0", i[1]+"\t\t"+i[2] + "\t\t" + i[4] + "\t\t" + i[6] + "\t" + i[8] + "/día \t\t" + str(i[10]) + "\t\t" + str(i[11])+"\t\t"+str(i[13])+"\t Sin comentario\n")
							duracion3 = i[13] + duracion3
			
			#print(datos_dict2)		
			#print(datos_dict2)
			#txbx.insert("1.0", datos_dict2)
			#txbx.bind("<Enter>", enter)
			
			texto = ""	
				
				
				#'Nombre\t\tModelo\t\tPlaca\t\tCosto/pensiont\tFecha ingreso\t\tFecha salida'
			#i = 3
			#var = np.size(lista)-i
			#b1 = str(b[0]).split('-')
			#b2 = str(b[1]).split('-')
			#a1 = ','.join(b1)
			#a2 = ','.join(b2)
			#b1_ = to_date(a1,VAR)
			#b2_ = to_date(a2,VAR)
			
			duracion1 = []
			duracion2 = []
			
			#txbx.delete('1.0','end')
			
			print("coches adentro: ",coches1, " coches afuera: ", coches2, " costo por cobra: ", cost, " costo recaudado: ", duracion3)
			
			#x,y = Total(duracion1,duracion2,duracion3,duracion4)
			tex = '    %i\n    %i'%(coches1, coches2)
			Label(window,  text=tex, font=(16), fg="red").grid(padx=5, pady=5, row=6, column=0,columnspan = 1, rowspan = 2,  sticky =  (E)) 
			tex = '        $%.2f \n        $%.2f'%(cost, duracion3)
			Label(window,  text=tex, font=(16), fg="red").grid(padx=5, pady=5, row=6, column=1, rowspan = 2,  sticky =  (E)) 
			#text = '______________________________________________________________________________________________________________\n'
			#txbx.insert('1.0',text)
			#print 'duracion :',duracion1,np.shape(duracion3),np.shape(duracion2)[0],np.shape(duracion4)
			"""tex = 'Hay:\t %i coches en modo por hora adentro \n\t %i coches en modo pension adentro \nSalieron:\t %i coches en modo por hora \n\t %i coches en modo pensión'%(np.size(duracion1), np.size(duracion3), np.size(duracion2), np.size(duracion4))
			"""
		else:
			status = Label(window, text="Consulta la lista de caracteres no admitidos en el menú Ayuda.", bd=1, relief=SUNKEN)
			status.grid(row=12,column=0,columnspan=4,rowspan=4)
			status.config(width=140)
		
	else:
		status = Label(tab2, text='Porfavor escribe el término a buscar', bd=1, relief=SUNKEN)

		status.grid(row=12,column=0,columnspan=4)
		status.config(width=140) 









































