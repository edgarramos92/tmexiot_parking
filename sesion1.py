import fileinput
import numpy as np  # instalable
#from Tkinter import *#instalable
from tkinter import ttk  # instalable
import mysql.connector
from tkinter import *
from tkinter.scrolledtext import ScrolledText
import datetime
from datetime import datetime
from math import *
import os
from sesion2 import sesion
from proy1_8 import stringchk2


#>>>>>>>>>>>>>>>>>> Conexion de mysql a la DB <<<<<<<<<<<<<<<<<<<<<<<<<
#<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
#Aquí se realiza la conexión a la base de datos Edgar_db, donde encontramos las tablas.

conn = mysql.connector.connect(
	host="localhost", user="edgarramos", passwd="Ganjahman92", database="tmexiot_db")
cursor = conn.cursor()
global valo2
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


def Login():
	global user, passw, rootA, svs
	svs = []

	rootA = Tk()
	rootA.title('Inicia Sesión en TMexIoT')
	status = Label(
		rootA, text="Bienvenido a TMexIoT, inicia sesión.", bd=1, relief=SUNKEN)

	status.grid(row=4, column=0, columnspan=4, rowspan=4)
	status.config(width=50)

	intruction = Label(
		rootA, anchor="e", text='Introduce los datos de inicio de sesión que te fueron \nproporcionados por correo electrónico.\n')
	intruction.grid(row=0, column=0, columnspan=2,  sticky=E)

	user_ = Label(rootA, text='Usuario: ')
	passw_ = Label(rootA, text='Contraseña: ')
	user_.grid(row=1, column=0, sticky=W)
	passw_.grid(row=2, column=0, sticky=W)

	user = Entry(rootA)
	passw = Entry(rootA, show='*')
	user.grid(row=1,  column=1)
	passw.grid(row=2, column=1)
	svs.append((user, passw))
	svs.append((user, passw))
	loginB = Button(rootA, text='Iniciar Sesión', command=CheckLogin)
	loginB.grid(column=0, row=3, sticky=(N, S, W, E))
	loginB = Button(rootA, text='Cancelar', command=(lambda: rootA.destroy()))
	loginB.grid(column=1, row=3,  sticky=(N, S, W, E))
	
	rootA.mainloop()


def CheckLogin():
	varstr2 = stringchk2(svs)
	print("varstr: ", varstr2)

	if varstr2:
		val = (user.get(), passw.get())
		cursor.execute("SELECT  * FROM tbl_tmexiot_user WHERE reg_user = %s and reg_pass = %s", val)
		res = cursor.fetchall()
		print(res)

		try:
			if np.size(res[0]) > 0:
				rootA.destroy()
				sesion(res, conn, cursor)
			else:
				status = Label(rootA, text="Datos incorrectos, vuelva a intentarlo", bd=1, relief=SUNKEN)

				status.grid(row=4, column=0, columnspan=4, rowspan=4)
				status.config(width=50)
		except:
			status = Label(rootA, text="Datos incorrectos, vuelva a intentarlo.", bd=1, relief=SUNKEN)

			status.grid(row=4, column=0, columnspan=4, rowspan=4)
			status.config(width=50)
			
	else:
		status = Label(rootA, text="Sólo puede utilizar números y letras.", bd=1, relief=SUNKEN)

		status.grid(row=4, column=0, columnspan=4, rowspan=4)
		status.config(width=50)	


Login()
